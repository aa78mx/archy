# archy

Automatic ArchLinux configuration and install desktop and software

## Usage

### Download

`sudo pacman -S git nano --noconfirm`

`git clone https://gitlab.com/archymedes/archy.git`

### Run

Uncomment jobs to compute. Available: 

- go_update_config. Must be run on /home/user
- go_arch. ArchLinux post-installation
- go_neuro. Install neuro software. Mendeley and Fiji (imageJ)
- go_R. Install R, RStudio and all packages
- go_latex. Install retext, texstudio and all latex packages

`nano archy/main.sh`

`chmod +x archy/main.sh && sh archy/main.sh`


