#!/bin/bash

# Remove previous config files and folders
# Exept Rpackages.R
mkdir archy/config
cd archy/config
rm -rf !(Rpackages.R)
cd ..
cd ..

# Create new config folder
mkdir config

# Update bashrc
cp .bashrc archy/config/bashrc

# Update pacman.conf
cp /etc/pacman.conf archy/config/pacman.conf

# Update sway config
mkdir -p archy/config/sway
cp .config/sway/config archy/config/sway/config

# Update kitty config
mkdir -p config/kitty
cp .config/kitty/kitty.conf archy/config/kitty/kitty.conf

# Update ranger
mkdir -p config/ranger
cp .config/ranger/rc.conf archy/config/ranger/rc.conf
cp .config/ranger/rifle.conf archy/config/ranger/rifle.conf

# Update rofi
mkdir -p config/rofi
cp .config/rofi/config archy/config/rofi/config

# Update waybar
mkdir -p config/waybar
cp .config/waybar/config archy/config/waybar/config
cp .config/waybar/style.css archy/config/waybar/style.css

