#!/bin/bash

# Delete user passord
sudo passwd -d $USER

# Sync repo
sudo pacman -Syy

# Rate mirrorlist
sudo pacman -S reflector --noconfirm
sudo reflector --verbose --latest 5 --sort rate --save /etc/pacman.d/mirrorlist
sudo pacman -Syy

# Uninstall vim
sudo pacman -Rsn vim --noconfirm

# Updpate system
sudo pacman -Syyu --noconfirm

# Install AUR-helper
sudo pacman -S git --noconfirm
cd ~
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
cd ~
rm -rf yay

# Copy bashrc
cp archy/config/bashrc .bashrc

# Copy pacman.conf
sudo cp archy/config/pacman.conf /etc/pacman.conf

# Install System apps
sudo pacman -S partitionmanager \
               gparted \
               exfat-utils \
               dosfstools \
               ark \
               unrar \
               zip \
               unzip \
               p7zip \
               ntfs-3g \
               grub-customizer \
               --noconfirm

# Add automount partitions
sudo mkdir /mnt/windows
sudo mkdir /mnt/DATOS
filename='/etc/fstab'
sudo echo '/dev/sda1 /mnt/windows ntfs defaults 0 0' >> $filename
sudo echo '/dev/sda3 /mnt/DATOS ntfs defaults 0 0' >> $filename
sudo echo '/dev/sda4 none swap defaults 0 0' >> $filename

# Install LXqt
sudo pacman -S lxqt --noconfirm

# Install KDE-Plasma
sudo pacman -S plasma-desktop \
               kdeplasma-addons \
               plasma-wayland-session \
               qt5ct \
               kde-gtk-config \
               powerdevil \
               network-manager-applet \
               plasma-nm \
               plasma-pa \
               breeze-gtk \
               breeze-gtk \
               kscreen \
               kate \
               dolphin \
               konsole \
               sddm-kcm \
               konqueror \
               kdeconnect \
               kompare \
               krename \
               kcalc \
               yakuake \
               filelight \
               --noconfirm

# Install Sway
sudo pacman -S sway \
               swaybg \
               swayidle \
               mako \
               --noconfirm

## Default sway config
mkdir .config/sway
cp /etc/sway/config .config/sway

## Copy sway config
cp archy/config/sway/config .config/sway/config

## Install kitty terminal
sudo pacman -S kitty --noconfirm

## Copy kitty config
mkdir .config/kitty
cp archy/config/kitty/kitty.conf .config/kitty/kitty.conf

## Install waybar
sudo pacman -S waybar otf-font-awesome --noconfirm

## Copy waybar config
mkdir .config/waybar
#cp /etc/xdg/waybar/config .config/waybar/config 
#cp /etc/xdg//waybar/style.css .config/waybar/style.css
cp archy/config/waybar/config .config/waybar/config 
cp archy/config/waybar/style.css .config/waybar/style.css

## Install ranger
sudo pacman -S ranger python-pillow --noconfirm

## Copy ranger config
ranger --copy-config=all
cp archy/config/ranger/rc.conf .config/ranger/rc.conf
cp archy/config/ranger/rifle.conf .config/ranger/rifle.conf

## Install rofi
sudo pacman -S rofi --noconfirm

## Copy rofi config
mkdir .config/rofi
cp archy/config/rofi/config .config/rofi/config

## Install swaylock
yay -S swaylock-effects-git --noconfirm

## Install redshift
yay -S redshift-wayland-git --noconfirm

## Install network dmenu
sudo pacman -S dmenu --noconfirm
yay -S networkmanager-dmenu-git --noconfirm

## Install reshift
yay -S redshift-wayland-git --noconfirm

# Install Web Browser
sudo pacman -S firefox \
               firefox-i18n-es-es \
               falkon \
               --noconfirm

yay -S google-chrome \
       brave-bin \
       --noconfirm

# Install torrent emule
sudo pacman -S transmission-qt \
               amule \
               --noconfirm

# Install Office
sudo pacman -S libreoffice-fresh \
               libreoffice-fresh-es \
               okular \
               --noconfirm

# Install Multimedia
sudo pacman -S vlc \
               kodi-x11 \
               --noconfirm
               
yay -S popcorntime-bin --noconfirm

# Install Graphics
sudo pacman -S blender \
               inkscape \
               gimp \
               krita \
               gwenview \
               spectacle \
               --noconfirm

# Install Megasync
yay -S megasync-bin --noconfirm

# Install manipulate images
sudo pacman -S imagemagick --noconfirm

# Install manipulate video
sudo pacman -S ffmpeg --noconfirm

# Install manipulate pdf
sudo pacman -S pdftk --noconfirm

# Install remote desktop
yay -S teamviewer --noconfirm
sudo systemctl enable teamviwerd

# Install webchat
sudo pacman -S telegram-desktop --noconfirm
yay -S rambox-bin zoom --noconfirm

# Install mendeley
yay -S mendeleydesktop-bundled --noconfirm

# Install miscelania
sudo pacman -S youtube-dl \
               htop \
               gtop \
               neofetch \
               cmatrix \
               --noconfirm

echo 'IMPORTANT:'
echo ''
echo '1- Check mounted partitions on /etc/fstab with sudo fdisk -l'
echo '2- Check GRUB entries with grub-customizer'
