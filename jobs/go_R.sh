#!/bin/bash

# Install R

sudo pacman -S r \
               tk \
               --noconfirm

# Install RStudio

yay -S rstudio-desktop-preview-bin --noconfirm

# Install R packages

Rscript archy/config/Rpackages.R
