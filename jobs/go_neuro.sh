#!/bin/bash

# Install mendeley and fiji (imageJ)
yay -S mendeleydesktop-bundled \
       fiji-bin \
       --noconfirm


