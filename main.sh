#!/bin/bash

# Convert job files to executable
#chmod +x archy/jobs/*.sh 

# Update configs
#sh archy/jobs/go_update_config.sh

# Custom. Install ready to use software
#sh archy/jobs/go_arch.sh

# Install neuro software. Mendeley and Fiji (imageJ)
#sh archy/jobs/go_neuro.sh

# Install R, RSTudio and all packages
#sh archy/jobs/go_R.sh

# Install texstudio and all latex packages
#sh archy/jobs/go_latex.sh
